## Einführung in die Programmierung mit Ruby

#### Kursziele

1. Wissen wie man Software Entwickelt und entwickeln sollte.
2. Werkzeuge und Kenntnisse haben um anzufangen, eigene Software zu entwickeln.
3. Wissen welche typischen Softwareentwicklungs Methoden es gibt und wie man sie richtig anwendet z.B. Testing, Plannung, Realisierung.
4. IT Werkzeugkiste haben um Probleme mit Hilfe der Informatik lösen zu können.
5. Typische Programmiersprachenkonstrukte kennen und wissen wie man sie in Ruby richtig anwendent.

#### Zeitplan

1. Einführung / Kennenlernen.
  - Modul 1: Programmierung
  - Modul 2: Software Entwicklung
2. Programmiersprachen:
  - Modul 3: Wie Programmiersprachen funktionieren
  - Modul 4: Kontrollstrukturen
3. Objekt-Orientierte Programmierung:
  - Modul 5: OOP (Klassen, Objekte)
  - Modul 6: Namensräume und Modulen
4. Softwarebau und Fehlerhandlung
  - Modul 7: Modularer Softwarebau
  - Modul 8: Debugging / Fehlerhandlung
  - Modul 9: Testing
5. IO, Codeverteilung, und Reguläre Ausdrücke
  - Modul 10: Dateien und Datenbanken
  - Modul 11: Verteilung von Ruby Software
  - Modul 12: Reguläre Ausdrücke
6. Hackathon Projekt
  - Modul 13: Startup Ideen und Prototyping Tools
