## Was ist die Software Entwicklung?

#### Lernziele

1. Wissen welche Arten von Apps und Anwendungen es gibt.
2. Lebenszyklus von Software kennen.
3. Sich mit der Versionskontrolle insb. Git auskennen.

#### Infos zur Vertiefung

##### 01 Software Entwicklung und Anwendungen

1. [6 Arten von Software](http://www.yuhiro.de/6-verschiedene-arten-von-software/)
2. [Softwarearten Taxonomie](http://einkauf.oesterreich.com/IV2_SS05/bestehendehp%FCberarbeitet/Softwarearten.htm)

##### 02 Lebenszyklus von Software

1. [Das uralte Wasserfall-Modell](http://de.ccm.net/contents/574-software-lebenszyklus)
2. [Scrum](https://www.openpm.info/display/openPM/Scrum+Lebenszyklus)
3. [Kanban](https://www.it-agile.de/wissen/einstieg-und-ueberblick/kanban/)

##### 03 Plannung
##### 04 Software Entwurf
##### 05 Implementierung, Testen und Dokumentation
##### 06 Bereitstellung und Wartung

1. [UML Tutorial](https://www.ibm.com/developerworks/rational/library/769.html)
2. [RSpec](http://rspec.info/)
3. [Cucumber Tutorial](https://www.engineyard.com/blog/cucumber-introduction)
4. [Behavior Driven Development](https://de.wikipedia.org/wiki/Behavior_Driven_Development)

##### 07 Versionskontrolle mit Git

1. [Git Grundlagen](https://git-scm.com/book/de/v1/Git-Grundlagen)
2. [Git Branching](https://learngitbranching.js.org/)
