## Übungen

#### 1 Einfaches Git Tutorial

Geh zu (try.github.io)[https://try.github.io/] und arbeite das Tutorial durch.

#### 2 Installiere ein Merge-Tool

Linux oder Windows - Installiere [Meld](http://meldmerge.org/)
Mac - installiere [XCode](https://developer.apple.com/download/) und benutze das FileMerge Tool.

#### Schreibe ein Gedicht

1. Man sollte die Repo: [MeinGedicht](https://github.com/jwaterfaucett/MeinGedicht) forken.
2. Man sollte dann die geforkte Repo bei sich klonen mit `git clone`.
3. Dann sollte man eine Datei im `gedichte` Verzeichnis erstellen und ein kurzes Gedicht schreiben.
4. Diese Änderungen sollten dann mit `git commit` lokal gespeichert werden.
5. Danach sollte mit `git push` die aktualierte Version, hoch zur geforkten Repo gepusht werden.
6. Wenn eine Person mit seinem Gedicht fertig ist sollte er ein Pull-Request auf github zur Originalrepo erstellen.
