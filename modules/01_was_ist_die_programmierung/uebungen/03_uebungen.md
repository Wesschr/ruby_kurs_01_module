Übungen 03
----------

Dein Ziel ist es einen Mann durch einen unbeleuchteten Labyrinth zu
navigieren.

Du hast ein Stuckpapier das du ihm geben kannst befor er ins Labyrinth
geht.

Du darfst aber nur 2 Zeichen und Zahlen benutzen.

1.  Vorwärts ein Block. `V`
2.  Dreh 90 Grad nach rechts. `D`
3.  Eine Zahl danach um eine Wiederholung zu representieren.

Beispiele:

V4 : Vorwärts 4 Blöcke D2 : 180 Grad Drehung.

Ein Biespiel für wie dein Skript aussehen sollte:

V2 D V7 ... usw.

Der Mann fängt an außerhalb des Labyrinths und mit seinem Gesicht nach
Osten gerichtet.

Benutze für die Aufgabe die Datei `alg-maze-exercise1.png`.
