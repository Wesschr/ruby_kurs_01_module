Übungen 02
----------

#### Aufgabe 1

Konvertiere die folgenden Wörter in ASCII Zahlen um:

    Katze
    Hund

#### Aufgabe 2

Konvertiere die Dezimal Zahlen aus Aufgabe 1 in Binärzahlen.
