## Übungen 01

1. Nimm kurz die Zeit und nenne alle Eingabe und Ausgabegeräte die es bei dir und deinem Computer/Laptop/Handy gibt.

2. Was für analoge Signale müssen bei den Geräten umgewandelt werden?

3. Was haben deine Computer / Laptop / Handy's für Betriebsysteme? Welche Versionen? Wie viel RAM-Speicher und was für ein CPU haben sie?

WINDOWS:
1. [System Information](https://www.google.de/search?q=Windows+systeminformation)
2. [Windows 10](https://www.windowscentral.com/how-check-your-computer-full-specifications-windows-10)


MAC:
```
From the Apple menu, select About This Mac.
```

LINUX:
```bash
# Betriebsystem und Version
cat /etc/issue

# cpu info
cat /proc/cpuinfo

# ram info
free -hm

# Festplatten und Partitionen
df -h
```

4. Wie viele User gibt es die sich bei den Geräten anmelden können?

LINUX:
```bash
# liste alle Login Users auf
who -q
```
