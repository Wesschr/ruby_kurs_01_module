Übungen 04
----------

Stell sicher das du Ruby installiert hast.

1.  Erstelle ein Record (Struct in Ruby) und spiele ein wenig damit rum.

``` {.ruby}
# erstelle den Bauplan für Personen
Person = Struct.new(:vorname, :nachname, :alter)

# instanziere eine Person
bob = Person.new("Bob", "Smith", 40)

# print seine Eigenschaften auf die Konsole
puts bob.vorname
puts bob.nachname

# aktualisere die Daten
bob.alter = 76
puts bob.alter
```

2.  Erstelle ein Array

``` {.ruby}

werte = [10,20,30,40]

# greife auf Array Elemente (0-indiziert)
puts werte[0]   # erstes Element
puts werte[3]   # letzes Element

# aendere die werte
werte[0] = 100

puts werte
```

3.  Erstelle ein Hash

``` {.ruby}

bob = { "vorname" => "Bob",
          "nachname" => "Smith",
          "alter" => 105 }

puts bob["vorname"]
puts bob["nachname"]

# bob hat Geburtstag!
bob["alter"] = 106

puts bob
```

4.  Erstelle ein Stack

``` {.ruby}

# Arrays bieten die gleiche funktionalitaet wie stacks
# LIFO Struktur (Last-In-First-Out)
stack = []
stack.push(2)
stack.push(3)

puts stack

puts stack.pop
puts stack
puts stack.pop
stack
```

5.  Erstelle ein Queue

``` {.ruby}

# Arrays koennen auch benutzt werden um queues/warteschlagen zu implementieren
# FIFO Struktur (First-In-First-Out)
queue = Queue.new
queue.push(10)
queue.push(50)
queue.push(99)

puts queue.pop
puts queue.pop
puts queue.pop

# was passiert wenn man nochmal queue.pop aufruft? warum?
```
